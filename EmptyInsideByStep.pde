class EmptyInsideByStep extends Step {

  protected PVector cvxHullPnt;
  protected PVector cvxHullPnt2;
  protected boolean nextPoint = true;
  protected int indxCorner = 0;
  protected int numberRemoved;
  protected boolean skipStep = false;
  protected boolean mainHullBeingPrcsd;

  EmptyInsideByStep(ConvexHull cvxHull) {
    super(cvxHull);
    mainHullBeingPrcsd = true;
  }
  
  EmptyInsideByStep(ConvexHull cvxHull, int idxCrn, boolean skpStp, PointCloud onTheLeft, int sizePolyToEmpty) {
    super(cvxHull, onTheLeft);
    this.indxCorner = idxCrn;
    indxCorner++;
    this.skipStep = skpStp;
    
    mainHullBeingPrcsd = false;
    
    this.convexHull = new ConvexHull();
    for (int i = 0 ; i < sizePolyToEmpty ; i++) {
      try {
        this.convexHull.add(this.mainConvexHull.get(idxCrn + i));
      } catch(IndexOutOfBoundsException e) {}
    }
    this.convexHullIterator = convexHull.listIterator();
  }

  public boolean update() {
    if ((pntsToProcessIterator.hasNext() || !nextPoint) && !skipStep) {

      cvxHullPnt = convexHullIterator.next();
      int indx = convexHullIterator.nextIndex();
      cvxHullPnt2 = convexHull.getPoint(indx);

      if (nextPoint) {
        beingPrcsdPoint = pntsToProcessIterator.next();
      }
      nextPoint = false;

      EnumSide side = EnumSide.whichSide(cvxHullPnt, cvxHullPnt2, beingPrcsdPoint);

      if (side == EnumSide.Left) {
        nextPoint = true;
      }
      if ((!convexHullIterator.hasNext()) || nextPoint) {
        convexHullIterator = convexHull.listIterator();
        if (!nextPoint) {
          unvalidPrcsdPnt.add(beingPrcsdPoint);
          if (mainHullBeingPrcsd) {
            pntsToProcessIterator.remove();
          } else {
           mainConvexHull.getPointsToProcess().remove(beingPrcsdPoint); 
          }
        }
        nextPoint = true;
      } else { 
        nextPoint = false;
      }
      return false;
    } else {
      if (indxCorner < mainConvexHull.size()) {
        mainConvexHull.setStep(new ExtrapolateEdgeByStep(mainConvexHull, indxCorner)); //<>//
        return false;
      } else {
        return true;
      }
    }
  }

  public void show() {
    super.show();
    stroke(0, 126, 255);
    strokeWeight(5);
    try {
      connect(cvxHullPnt, cvxHullPnt2);
      connect(cvxHullPnt, beingPrcsdPoint);
    } 
    catch (NullPointerException e) {
    } //<>//
  }
}
