abstract class Step {

  protected ConvexHull mainConvexHull;
  protected ConvexHull convexHull;
  protected PointCloud unvalidPrcsdPnt;
  protected PointCloud pntsToProcess;
  protected int sizeToPrcs;
  protected ListIterator<PVector> convexHullIterator;
  protected ListIterator<PVector> pntsToProcessIterator;
  protected PVector beingPrcsdPoint;

  Step() {
  }
  Step(ConvexHull cnvxHull) {
    stepConstructor(cnvxHull, cnvxHull.getPointsToProcess());
    this.convexHull = mainConvexHull;
    this.convexHullIterator = convexHull.listIterator();
  }
  
  Step(ConvexHull cnvxHull, PointCloud toProcess) {
    stepConstructor(cnvxHull, toProcess);
  }
  
  private void stepConstructor(ConvexHull cnvxHull, PointCloud toProcess) {
    this.mainConvexHull = cnvxHull;
    this.unvalidPrcsdPnt = mainConvexHull.getUnvalidProcessedPoints();
    this.pntsToProcess = toProcess;
    this.pntsToProcessIterator = pntsToProcess.listIterator();
    this.sizeToPrcs = pntsToProcess.size();    
  }

  public abstract boolean update();
  public void show() {
    stroke(0, 126, 255);
    strokeWeight(20);
    if (pntsToProcessIterator.hasNext()) {
      try {
        point(beingPrcsdPoint.x, beingPrcsdPoint.y);
      } 
      catch (NullPointerException e) {
      }
    }
  }
}
